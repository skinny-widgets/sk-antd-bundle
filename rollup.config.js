
import babel from '@rollup/plugin-babel';
import nodeResolve from '@rollup/plugin-node-resolve';
import commonJS from '@rollup/plugin-commonjs';

export default [
  {
    input: 'index.js',
    output: {
      globals: {
      },
      file: 'dist/sk-antd-bundle.js',
      format: 'umd',
      name: 'window',
      extend: true,
      sourcemap: true,
    },
    external: [  ],
    plugins: [
      nodeResolve({ mainFields: ['jsnext:main']}),
      commonJS({ include: "node_modules/**" }),
      babel({ babelHelpers: 'runtime' }),
    ]
  },
  {
    input: './node_modules/sk-core/src/sk-registry-init.js',
    output: {
      globals: {
      },
      file: 'dist/sk-registry-init-bundle.js',
      format: 'umd',
      name: 'window',
      extend: true,
      sourcemap: true,
    },
    external: [  ],
    plugins: [
      nodeResolve({ mainFields: ['jsnext:main']}),
      commonJS({ include: "node_modules/**" }),
      babel({ babelHelpers: 'runtime' }),
    ]
  }

];
