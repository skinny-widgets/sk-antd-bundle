# Skinny Widgets Antd Theme Bundle

autonomous skinny-widgets bundle shipped with antd theme

```
npm i sk-antd-bundle --save
```


then add the following to your html

```html
<sk-config
    theme="antd"
></sk-config>

<sk-input>Input</sk-input>
<script src="/node_modules/sk-antd-bundle/dist/sk-antd-bundle.js"></script>
<script type="module">
    customElements.define('sk-input', SkInput);
</script>
```

you can register elements you use with **<sk-registry>** 

```html
<html class="sk-auto-reg">

<body>
<script src="/node_modules/sk-antd-bundle/dist/sk-antd-bundle.js"></script>
<script type="module" src="/node_modules/sk-core/src/sk-registry-init.js"></script>

<sk-config
    theme="antd"
></sk-config>
<sk-input>Input</sk-input>

<sk-registry>
    <sk-registry-item key="SkInput" defName="SkInput" is="sk-input"></sk-registry-item>
</sk-registry>
</body>
</html>
```

## Development

You can go with just WHATWG modules for development purposes, but may need some extra tools.

### Hot rebuilding files

relying on the idea all dependant modules checkouted to parent watch can run rebuild bundle and copy to stand project
automatically on changed. It can be helpful when you need to debug or develop some related to bundles functionality.

```
sk/ 

    sk-input/

    sk-input-antd/

    sk-antd-bundle/

    sk-stand/
```

then run

```
npm run watch
```

### Versions update

when you are done with changes you may need to check and update all the versions. Update script relies on
structure described above.

```
npm run vbump 0.2.2
```